-- src/Main.elm: main module for the Elm application
--
-- Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY
-- This is free software, and you are welcome to redistribute it and/or modify
-- it under the terms of the BSD 2-clause License. See the LICENSE file for more
-- details.
--


port module Main exposing (main)

import Browser
import Browser.Navigation
import FormatNumber
import FormatNumber.Locales exposing (Locale)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onFocus, onInput)
import Url exposing (Url)



-- VIEW


view : Model -> Browser.Document Msg
view model =
    let
        totalText =
            model.denominations
                |> List.map (Tuple.mapFirst denominationAmount)
                |> List.map (\( amount, count ) -> amount * count)
                |> List.sum
                |> formatRupee

        header_ =
            header []
                [ div [ class "navbar navbar-dark fixed-top bg-dark" ]
                    [ div [ class "title" ]
                        [ img [ src "/assets/img/icons/favicon-32x32.png", alt "" ] []
                        , strong [ class "text-white" ] [ text "පොකැට්\u{200D}" ]
                        ]
                    ]
                ]

        footer_ =
            footer [ class "mt-auto py-3 bg-dark" ]
                [ div [ class "container text-light text-center" ]
                    [ strong [] [ text ("Pokaet - " ++ model.flags.version) ]
                    , br [] []
                    , small [ class "d-none d-sm-inline" ]
                        [ text "Copyright 2019 - "
                        , a [ href "https://sudaraka.org/", target "_new_win" ] [ text "Sudaraka Wijesinghe" ]
                        , text " - BSD 2-Clause license - "
                        , a [ href "https://bitbucket.org/sudaraka/pokaet-app", target "_new_win" ] [ text "source" ]
                        , text "."
                        ]
                    ]
                ]

        body_ =
            node "main"
                [ class "container" ]
                ((model.denominations |> List.map renderDenomination)
                    ++ [ div [ class "jumbotron bg-success text-white" ]
                            [ h1 [ title totalText ] [ text totalText ]
                            , if "" == totalText then
                                text ""

                              else
                                button
                                    [ type_ "button"
                                    , class "btn btn-light btn-md"
                                    , onClick ClearAll
                                    ]
                                    [ text "×" ]
                            ]
                       ]
                )
    in
    { title = "පොකැට්", body = [ header_, body_, footer_ ] }


renderDenomination : DenominationCount -> Html Msg
renderDenomination ( denomination, count ) =
    let
        amount =
            denomination |> denominationAmount

        input_id =
            "input_" ++ (amount |> String.fromInt)
    in
    div [ class "input-group input-group-lg mb-1" ]
        [ label [ class "input-group-prepend", for input_id ]
            [ span [ class "input-group-text bg-secondary text-white" ] [ text (amount |> formatRupee) ]
            ]
        , input
            [ type_ "number"
            , id input_id
            , class "form-control"
            , attribute "min" "0"
            , step "1"
            , value (count |> String.fromInt)
            , onInput (CountChange denomination)
            , onFocus (SelectContent amount)
            ]
            []
        , div [ class "input-group-append bg" ]
            [ span [ class "input-group-text" ] [ text ((amount * count) |> formatRupee) ]
            ]
        , div [ class "input-group-append clear" ]
            [ button
                [ type_ "button"
                , class "btn btn-outline-secondary"
                , onClick (CountChange denomination "0")
                , disabled (0 == count)
                ]
                [ text "×" ]
            ]
        ]


formatRupee : Int -> String
formatRupee value =
    let
        locale =
            Locale 0 "," "" "රු" "" "රු" ""
    in
    if 1 > value then
        ""

    else
        value |> toFloat |> FormatNumber.format locale


denominationAmount : Denomination -> Int
denominationAmount denomination =
    case denomination of
        FiveThousand ->
            5000

        OneThousand ->
            1000

        FiveHundred ->
            500

        OneHundred ->
            100

        Fifty ->
            50

        Twenty ->
            20



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LinkClicked _ ->
            ( model, Cmd.none )

        UrlChanged _ ->
            ( model, Cmd.none )

        CountChange denomination value ->
            let
                denominations =
                    model.denominations
                        |> List.map
                            (setDenominationCount denomination
                                (value
                                    |> String.toInt
                                    |> Maybe.withDefault 0
                                )
                            )
            in
            ( { model | denominations = denominations }, Cmd.none )

        SelectContent id ->
            ( model, selectInput id )

        ClearAll ->
            ( initialState model.flags, Cmd.none )


setDenominationCount : Denomination -> Int -> DenominationCount -> DenominationCount
setDenominationCount denomination count ( d, c ) =
    if denomination == d then
        ( denomination, count )

    else
        ( d, c )



-- MODEL


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url
    | CountChange Denomination String
    | SelectContent Int
    | ClearAll


type Denomination
    = FiveThousand
    | OneThousand
    | FiveHundred
    | OneHundred
    | Fifty
    | Twenty


type alias Flags =
    { version : String }


type alias DenominationCount =
    ( Denomination, Int )


type alias Model =
    { flags : Flags
    , denominations : List DenominationCount
    }



-- INIT


initialState : Flags -> Model
initialState flags =
    Model flags
        [ ( FiveThousand, 0 )
        , ( OneThousand, 0 )
        , ( FiveHundred, 0 )
        , ( OneHundred, 0 )
        , ( Fifty, 0 )
        , ( Twenty, 0 )
        ]


init : Flags -> Url -> Browser.Navigation.Key -> ( Model, Cmd Msg )
init flags url key =
    ( initialState flags, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- PORT


port selectInput : Int -> Cmd msg



-- MAIN


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = LinkClicked
        , onUrlChange = UrlChanged
        }
