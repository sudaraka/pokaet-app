
/* src/sw.js: service worker for pokaet app
 *
 * Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

const
  CACHE_ID = `pokaet-${REVISION}`,

  ASSETS =
    [ '/'
    , ...serviceWorkerOption.assets
    ]

// SW install: cache all assets
self.addEventListener('install', e => {
  e.waitUntil(
    caches
      .open(CACHE_ID)
      .then(cache => cache.addAll(ASSETS))
  )
})

// SW activate: remove all caches except the current one
self.addEventListener('activate', e => {
  const
    caches_cleaned = caches
      .keys()
      .then(keys => {
        keys.forEach(key => {
          if(CACHE_ID !== key && key.match('pokaet-')) {
            return caches.delete(key)
          }
        })
      })

  e.waitUntil(caches_cleaned)
})

// SW fetch: respond from cache & use network as fallback
self.addEventListener('fetch', e => {
  e.respondWith(
    caches
      .match(e.request)
      .then(cached_response => {
        if(cached_response) {
          return cached_response
        }

        return fetch(e.request)
          .then(network_response => {
            caches
              .open(CACHE_ID)
              .then(cache => cache.add(e.request, network_response))

            return network_response.clone()
          })
      })
  )
})
