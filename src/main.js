/* src/main.js: entry point Js module for pokaet app
 *
 * Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

'use strict'

import './styles.sass'
import { Elm } from './Main.elm'

const
  app = Elm.Main.init({ 'flags': { 'version': VERSION } })

// Select content of input box on ports signal sent from Elm
app.ports.selectInput.subscribe(id => {
  document.getElementById(`input_${id}`).select()
})

// Register service worker when available
if(navigator.serviceWorker) {
  navigator.serviceWorker
    .register('/sw.js')
    .catch(console.error)
}
