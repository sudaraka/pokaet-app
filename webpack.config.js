/* webpack-config.js: Webpack configuration
 *
 * Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

const
  { resolve } = require('path'),
  merge = require('webpack-merge'),
  Html = require('html-webpack-plugin'),
  Copy = require('copy-webpack-plugin'),
  ServiceWorker = require('serviceworker-webpack-plugin'),
  CSS = require('mini-css-extract-plugin')

module.exports = (_, argv) => {
  const
    debug =  argv.d || argv.debug,

    default_config =
      { 'entry': { 'assets/js/app.js': resolve(__dirname, 'src/main.js') }

      , 'output':
        { 'path': resolve(__dirname, 'dist/')
        , 'filename': '[name]'
        }

      , 'module':
        { 'rules':
          [ { 'test': /\.elm$/u
            , 'exclude': [ /elm-stuff/u, /node_modules/u ]
            , 'use':
              { 'loader': 'elm-webpack-loader'
              , 'options':
                { debug
                , 'optimize': !debug
                }
              }
            }
          , { 'test': /\.sass$/u
            , 'use':
            [ { 'loader': CSS.loader
              , 'options':
                { 'hmr': debug
                , 'reloadAll': true
                }
              }
              , 'css-loader'
              , 'sass-loader'
              ]
            }
          ]
        }

      , 'plugins':
        [ new Html
          (
            { 'template': resolve(__dirname, 'src/layout.ejs')
            , 'filename': 'index.html'
            , 'minify': debug ? false :
              { 'collapseBooleanAttributes': true
              , 'collapseWhitespace': true
              , 'decodeEntities': true
              , 'html5': true
              , 'minifyCSS': true
              , 'minifyJS': true
              , 'removeAttributeQuotes': true
              , 'removeComments': true
              , 'removeRedundantAttributes': true
              , 'removeScriptTypeAttributes': true
              , 'removeStyleLinkTypeAttributes': true
              , 'useShortDoctype': true
              }
            }
          )

        , new Copy
          ( [ { 'from': 'assets/'
              , 'to': 'assets/'
              , 'ignore':
                [ 'root/**/*'
                ]
              }
            , { 'from': 'assets/root/*'
              , 'flatten': true
              }
            ]
          )

        , new ServiceWorker
          ( { 'entry': resolve(__dirname, 'src/sw.js')
            }
          )

        , new CSS
          ( { 'filename': 'assets/css/styles.css'
            }
          )
        ]
      },

    dev_config = debug ? {} : {}

  return merge(default_config, dev_config)
}
