# Makefile: build script for pokaet app
#
# Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
#
# This program comes with ABSOLUTELY NO WARRANTY
# This is free software, and you are welcome to redistribute it and/or modify
# it under the terms of the BSD 2-clause License. See the LICENSE file for more
# details.
#

HOST ?=127.0.0.1
PORT ?=5000
SRC_DIR =src
DIST_DIR =dist
WEBPACK_CONFIG =webpack.config.js

all: build

build: ${WEBPACK_CONFIG} ${SRC_DIR}/**/*
	npx webpack \
		-p --mode=production \
		--hide-modules \
		--progress \
		--define REVISION="\"`git rev-parse --short HEAD`\"" \
		--define VERSION="\"`git describe --tags --abbrev=0 2>/dev/null`\"" \
		--config $<

.PHONY: run-server clean

run-server: webpack.config.js
	npx webpack-dev-server \
		--host ${HOST} \
		--port ${PORT} \
		--mode development \
		--debug \
		--hot \
		--define REVISION="\"`cat /dev/urandom|head |sha1sum|cut -c -6`\"" \
		--define VERSION="\"`git describe --tags --abbrev=0 2>/dev/null`\"" \
		--config ${WEBPACK_CONFIG}

clean:
	$(RM) -r ${DIST_DIR}/

%:
	@:
